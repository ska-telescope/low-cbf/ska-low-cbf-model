# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# All rights reserved

"""Read HBM dump data and compare with Model"""

import argparse

import numpy as np


def create_argument_parser() -> argparse.ArgumentParser:
    """Configure command-line"""
    parser = argparse.ArgumentParser(description="RAW to PCAP")

    #
    parser.add_argument(
        "alveo_vis",
        type=argparse.FileType("rt"),
        help="Downloaded visibility data from correlator output HBM",
    )
    # default name output by the model is "LTA_vis_check.txt"
    parser.add_argument(
        "model_vis",
        type=argparse.FileType("rt"),
        help="Model visibility data file",
    )
    """
    # default name output by the model is "LTA_TCI_FD_check.txt"
    parser.add_argument(
        "check_tcifd", type=argparse.FileType("rt"), help="Downloaded tci/fd data from correlator output HBM"
    )
    parser.add_argument(
        "model_tcifd", type=argparse.FileType("rt"), help="Model tci,fd data file"
    )
    """
    return parser


def main():

    parser = create_argument_parser()
    args = parser.parse_args()

    model_vis = []
    for dline in args.model_vis:
        dlist = dline.split()
        # first word in a line is the address, rest are the data.
        # dlist_addr = int(dlist.pop(0), 16)
        dlist.pop(0)
        for dword in dlist:
            model_vis.append(int(dword, 16))

    # check data is just a 32-bit word on each line
    alveo_vis = []
    for dline in args.alveo_vis:
        alveo_vis.append(int(dline, 16))

    model_vis_uint32 = np.array(model_vis, dtype=np.uint32)
    model_vis_fp = np.frombuffer(
        model_vis_uint32.tobytes(), dtype=np.complex64
    )
    alveo_vis_uint32 = np.array(alveo_vis, dtype=np.uint32)
    alveo_vis_fp = np.frombuffer(
        alveo_vis_uint32.tobytes(), dtype=np.complex64
    )

    # compare 16x16 station cells.
    # each 16x16 station cell is 8192 bytes = 1024 visibilities
    stations = 6
    max_abs = 0
    min_abs = 2
    dev_from_1_max = 0
    dev_from_1_sum = 0
    dev_from_1_count = 0
    alveo_cells = alveo_vis_fp.shape[0] // 1024
    model_cells = model_vis_fp.shape[0] // 1024
    print(
        f"Alveo cells recorded = {alveo_cells}, model cells recorded = {model_cells}"
    )
    for cell in range(np.min((alveo_cells, model_cells))):
        vis_ratios = np.zeros((stations, stations * 4), dtype=np.complex64)
        for row in range(stations):
            for col in range((row + 1) * 4):
                # first col has 4 values, second 8, third 12...
                vis_ratios[row, col] = (
                    alveo_vis_fp[cell * 1024 + row * 64 + col]
                    / model_vis_fp[cell * 1024 + row * 64 + col]
                )
                dev_from_1_sum = dev_from_1_sum + np.abs(
                    vis_ratios[row, col] - 1
                )
                dev_from_1_count = dev_from_1_count + 1
                if np.abs(vis_ratios[row, col]) > max_abs:
                    max_abs = np.abs(vis_ratios[row, col])
                if np.abs(vis_ratios[row, col]) < min_abs:
                    min_abs = np.abs(vis_ratios[row, col])
                if np.abs(vis_ratios[row, col] - 1) > dev_from_1_max:
                    dev_from_1_max = np.abs(vis_ratios[row, col] - 1)
        # print(vis_ratios[0:stations,0:(4*stations)])

    print(
        f"max_abs = {max_abs}, min_abs = {min_abs}, max deviation from ratio of 1 = {dev_from_1_max}"
    )
    print(
        f"Average deviation from ratio of 1 = {dev_from_1_sum/dev_from_1_count}"
    )


if __name__ == "__main__":
    main()
