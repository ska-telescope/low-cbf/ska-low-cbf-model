# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf_conn"""

name = """ska_low_cbf_model"""
version = "0.1.2"
version_info = version.split(".")
description = """SKA Low CBF Model to generate test vector and expected
outcome from the correlator and beamformer."""
author = "CSIRO"
author_email = ""
license = """CSIRO Open Source Licence"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""
