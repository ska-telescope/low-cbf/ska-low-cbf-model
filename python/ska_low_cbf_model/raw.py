# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Read & write the "RAW" Ethernet packet format.
"""
from decimal import Decimal
from typing import BinaryIO, Iterable, Tuple, Union

import dpkt
import numpy as np

raw_header = np.dtype(
    [
        ("data_offset", ">u8"),
        ("bytes", ">u4"),
        (
            "time",
            ">u8",
        ),  # time, in ns, to send the packet. Relative to the first packet.
        ("reserved", "B", 12),
        ("ethernet_header", "B", 14),
        ("ipv4_header", "B", 20),
        ("udp_header", "B", 8),
        # TODO: in v3 the SPEAD header is 56 bytes. We could use the "unused_alignment"
        #  field to flag v2/3, or to record used header size (max 72), or just make a
        #  clean break and only support v3 at some future point when/if the model itself
        #  is updated to handle v3...
        ("spead_header", "B", 72),
        ("unused_alignment", "B", 2),
    ]
)

ZERO_DATA_OFFSET = 0xFFFFFFFFFFFFFFFF


class RawDataParser:
    """RAW Decoder Class"""

    def __init__(self, headers: np.ndarray, data: np.ndarray):
        self._headers = headers
        self._data = data
        self._iter_index = 0

    def __getitem__(self, item: int) -> (np.ndarray, np.ndarray):
        header = self._headers[item]
        data_offset = header["data_offset"]
        data_bytes = header["bytes"]
        if data_offset == ZERO_DATA_OFFSET:
            data = np.zeros(data_bytes, dtype=np.uint8)
        elif data_offset >= len(self._data):
            raise ValueError("Header offset exceeds size of data")
        else:
            data = self._data[data_offset : (data_offset + data_bytes)]
        return np.concatenate(
            (
                header["ethernet_header"],
                header["ipv4_header"],
                header["udp_header"],
                header["spead_header"],
                data,
            )
        ), (header["time"] / 1e9)

    def __iter__(self):
        self._iter_index = -1
        return self

    def __next__(self):
        self._iter_index += 1
        if self._iter_index >= len(self._headers):
            raise StopIteration
        return self[self._iter_index]

    def __len__(self):
        return len(self._headers)


def write_raw(
    header_file: BinaryIO,
    data_file: BinaryIO,
    packets: Iterable[Tuple[Union[float, Decimal], bytes]],
):
    """
    Write packets to 'raw' header & data files.

    :param header_file: file object to write raw headers to.
    :param data_file: file object to write raw data to.
    :param packets: (timestamp in seconds, packet data) tuples.

    :returns: None
    """

    spead_header_size = raw_header["spead_header"].itemsize

    first_timestamp = None
    for (timestamp, data) in packets:
        if first_timestamp is None:
            first_timestamp = timestamp
        header = np.zeros(1, dtype=raw_header)

        eth = dpkt.ethernet.Ethernet(data)
        assert isinstance(eth, dpkt.ethernet.Ethernet)
        udp = None
        if eth.type == dpkt.ethernet.ETH_TYPE_IP:
            ip = eth.data
            if ip.p == dpkt.ip.IP_PROTO_UDP:
                udp = ip.data

        if udp is None:
            raise TypeError("Wrong type of packet")

        header["bytes"] = len(udp.data) - spead_header_size
        header["ethernet_header"] = np.frombuffer(eth.pack_hdr(), dtype="B")
        header["ipv4_header"] = np.frombuffer(ip.pack_hdr(), dtype="B")
        header["udp_header"] = np.frombuffer(udp.pack_hdr(), dtype="B")
        header["spead_header"] = np.frombuffer(
            udp.data[:spead_header_size], dtype="B"
        )
        data = udp.data[spead_header_size:]
        if np.count_nonzero(data) == 0:
            # if data is all zero
            header["data_offset"] = ZERO_DATA_OFFSET
        else:
            header["data_offset"] = data_file.tell()
            data_file.write(data)
        # time to send the packet, relative to the first packet, in ns.
        header["time"] = (timestamp - first_timestamp) * 1_000_000_000
        header_file.write(header.tobytes())
