import time


def display_status(fpga):
    # Read FPGA registers and report status

    # Build date, version and uptime
    args_date = fpga.system.args_map_build.value
    args_year = args_date // 2**24
    args_month = (args_date // 2**16) & 0xFF
    args_day = (args_date // 2**8) & 0xFF
    args_hour = args_date & 0xFF
    major_ver = fpga.system.firmware_major_version.value
    minor_ver = fpga.system.firmware_minor_version.value
    patch_ver = fpga.system.firmware_patch_version.value
    label = fpga.system.firmware_label.value
    personality = fpga.system.firmware_personality.value.to_bytes(4, "big")
    uptime = fpga.system.time_uptime.value
    print(
        f"Build year:20{args_year:x}, month:{args_month:x}, day:{args_day:x}, hour:{args_hour:x} "
    )
    print(
        f"Version : {major_ver}.{minor_ver}.{patch_ver}.{label}, Personality = {personality}, uptime = {uptime}"
    )

    # 100G Ethernet
    locked = fpga.system.eth100g_locked.value
    rx_packets = fpga.system.eth100g_rx_total_packets.value
    tx_packets = fpga.system.eth100g_tx_total_packets.value
    rx_bad_fcs = fpga.system.eth100g_rx_bad_fcs.value
    rx_bad_code = fpga.system.eth100g_rx_bad_code.value
    if locked:
        print(
            f"100GE up, RX packets (i.e. into FPGA) = {rx_packets}, tx packets = {tx_packets}, bad rx = {rx_bad_fcs + rx_bad_code}"
        )
    else:
        print("!!!! 100GE is not locked")
    print("----------------")

    # LFAA ingest
    channels = fpga.lfaadecode100g.total_channels.value
    vctable = fpga.lfaadecode100g.vctable.value
    vctable_vc = vctable[1::2]
    max_virtual_channel = vctable_vc.max()
    SPEAD_packets = fpga.lfaadecode100g.spead_packet_count.value
    nonspead_packets = fpga.lfaadecode100g.nonspead_packet_count.value
    badEth = fpga.lfaadecode100g.badethernetframes.value
    badIPUDP = fpga.lfaadecode100g.badipudpframes.value
    noVirtualChannel = fpga.lfaadecode100g.novirtualchannelcount.value
    print(f"Maximum virtual channel in vc table = {max_virtual_channel}")
    print(
        f"LFAA decode configured for {channels} virtual channels (Note this is rounded up to a multiple of 4)"
    )
    print(
        f"First 4 words (=2 entries) of VC table = {vctable[0]:x}, {vctable[1]:x}, {vctable[2]:x}, {vctable[3]:x}"
    )
    print(
        f"LFAA ingest packets: SPEAD = {SPEAD_packets}, non-SPEAD = {nonspead_packets}, malformed = {badEth + badIPUDP}, No VCtable match = {noVirtualChannel} "
    )
    print("----------------")

    # Corner turn 1
    ct1_running = fpga.corr_ct1.running.value
    ct1_reset = fpga.corr_ct1.full_reset.value
    ct1_buf0_frame = fpga.corr_ct1.frame_count_buffer0.value
    ct1_buf1_frame = fpga.corr_ct1.frame_count_buffer1.value
    ct1_buf2_frame = fpga.corr_ct1.frame_count_buffer2.value
    ct1_late = fpga.corr_ct1.input_late_count.value
    ct1_early = fpga.corr_ct1.input_too_soon_count.value
    ct1_duplicates = fpga.corr_ct1.duplicates.value
    ct1_missing = fpga.corr_ct1.missing_blocks.value
    ct1_output_counts = fpga.corr_ct1.correlator_output_count.value

    if ct1_running:
        print("Corner Turn 1 is Running")
    else:
        print("Corner turn 1 is Not Running")
    print(f"CT1 reset status = {ct1_reset}")
    print(
        f"buf0 frame = {ct1_buf0_frame}, buf1 frame = {ct1_buf1_frame}, buf2 frame = {ct1_buf2_frame}"
    )
    print(
        f"Packet counts : Late = {ct1_late}, early = {ct1_early}, duplicates = {ct1_duplicates}, readout missing = {ct1_missing}"
    )
    print(
        f"Output counts for first 8 virtual channels = {ct1_output_counts[0]}, {ct1_output_counts[1]}, {ct1_output_counts[2]}, {ct1_output_counts[3]}, {ct1_output_counts[4]}, {ct1_output_counts[5]}, {ct1_output_counts[6]}, {ct1_output_counts[7]} "
    )
    print("----------------")

    # filterbank
    fb2ct2 = fpga.filterbanks.txcount_corner_turn.value
    print(f"Packets sent from filterbank to corner turn 2 = {fb2ct2}")
    print("----------------")

    # Corner turn 2
    ct2_readinclocks = fpga.corr_ct2.readinclocks.value
    ct2_readinAllclocks = fpga.corr_ct2.readinallclocks.value
    ct2_readoutBursts = fpga.corr_ct2.readoutbursts.value
    ct2_readoutFrames = fpga.corr_ct2.readoutframes.value

    ct2_framesReceived = fpga.corr_ct2.framecountin.value
    ct2_status = fpga.corr_ct2.status0.value
    ct2_din_status1 = fpga.corr_ct2.din_status1.value
    ct2_din_status2 = fpga.corr_ct2.din_status2.value

    print(
        f"Recent interval between filterbank frames (nomimal value 283 ms) = {ct2_readinclocks * 3.333e-6} ms"
    )
    print(
        f"Number of full frames received from filterbank = {ct2_framesReceived}"
    )
    print(
        f"Recent interval between triggers to correlator processing (nominal value 849ms) = {ct2_readinAllclocks * 3.333e-6} ms"
    )
    print(f"Number of correlator frames triggered = {ct2_readoutFrames}")
    print(
        f"Packets sent to correlator (1 packet = 32 times x 4 stations)  = {ct2_readoutBursts}"
    )

    stat_rst = ct2_status & 0x1
    stat_datavalid = (ct2_status >> 1) & 0x1
    stat_sofHold = (ct2_status >> 2) & 0x1
    stat_framecountMod3 = (ct2_status >> 3) & 0x3
    stat_vc = ct2_status >> 16
    print(
        f"ct2 status {ct2_status:x}, rst={stat_rst},dataValid={stat_datavalid},sofhold={stat_sofHold},frameMod3={stat_framecountMod3},VC={stat_vc} "
    )

    print(
        f"din status1 {ct2_din_status1:x}, copyToHBMcount={(ct2_din_status1 & 0xffff)}, copy_fsm={((ct2_din_status1>>16) & 0xf)}, copydata_fsm={((ct2_din_status1>>20) & 0xf)}, fifo datacount={((ct2_din_status1>>24) & 0x3f)}, first_aw={((ct2_din_status1>>30) & 0x1)}, copy_fsm_stuck={((ct2_din_status1>>31) & 0x1)} "
    )
    print(
        f"din status2 {ct2_din_status2:x}, copyData_count={(ct2_din_status2 & 0xffff)}, copy_fsm_dbg_at_start={((ct2_din_status2>>16) & 0xf)}, copy_finechannel={((ct2_din_status2>>20) & 0xfff)}"
    )

    # Readout
    corr_HBM_write_ptr = fpga.config.cor0_hbm_end.value
    print(
        f"16x16 station correlator blocks written to HBM by correlator (never resets) = {corr_HBM_write_ptr//8192}"
    )

    packetiser_enable = fpga.spead_reg.enable_packetiser.value
    print(f"packetiser enable = {packetiser_enable}")
    rd_it_fsm = fpga.hbm_rd_debug.debug_pack_it_fsm.value
    rd_tri_fsm = fpga.hbm_rd_debug.debug_cor_tri_fsm.value
    rd_rd_fsm = fpga.hbm_rd_debug.debug_hbm_reader_fsm.value
    rd_instr_writes = fpga.hbm_rd_debug.subarray_instruction_writes.value
    print(
        f"SPEAD HBM read : it fsm = {rd_it_fsm}, tri fsm = {rd_tri_fsm}, reader_fsm = {rd_rd_fsm}, subarray instruction write count = {rd_instr_writes}"
    )


def display_status_forever(fpga, sleep_time):
    while True:
        display_status()
        time.sleep(sleep_time)
