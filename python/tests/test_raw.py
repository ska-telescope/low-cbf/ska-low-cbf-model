# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=R0201
# pylint: disable=R0903
"""Test RAW handler"""

from ska_low_cbf_model import raw


class TestHeader:
    """Verify RAW header"""

    def test_len(self):
        """Check size of header"""
        assert raw.raw_header.itemsize == 148
