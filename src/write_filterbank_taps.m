% Generate text files with the coefficients for the filterbanks.

%% WARNING :
% There is an equivalent piece of code in the firmware repository :
%  low-cbf-firmware/libraries/signalProcessing/filterbanks/src/matlab/get_row_coefficients
% which generates the .coe files to initialise the memories used in the firmware for the filterbanks.
%

PSSFilterbankTaps = (round(2^17 * generate_MaxFlt(64,12)));   % PSS, 64 point FFT, 12 taps.
PSTFilterbankTaps = (round(2^17 * generate_MaxFlt(256,12)));  % PST, 256 point FFT, 12 taps
correlatorFilterbankTaps = (round(2^17 * generate_MaxFlt(4096,12))); % Correlator, 4096 point FFT, 12 taps.

save('pss-filter.txt','PSSFilterbankTaps', '-ascii');
save('pst-filter.txt','PSTFilterbankTaps', '-ascii');
save('correlator-filter.txt','correlatorFilterbankTaps', '-ascii');


