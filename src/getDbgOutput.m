function getDbgOutput(rundir,dbgType,sourceFPGA,destinationFPGA)
%% Post process the model output to get debug port data.
% For firmware versions where the debug output UDP port is 0x5679
%
% Inputs :
%  dbgType = Selects which stage in the processing to capture.
%           1 -> even virtual channels at the output of doppler correction in first stage processing.
%           2 -> odd virtual channels at the output of the doppler correction in the first stage processing.
%                (even and odd packets can be selected since they are processing separately in the firmware)
%           3 -> input to the corner turn. "destinationFPGA" input selects the destination FPGA (e.g. 1 to 3 for PISA)
%           others -> unsupported as yet.
%  sourceFPGA = which FPGA to process for (e.g. 1 to 3 for PISA). NOTE : Assumes XYZ coordinate, with 4 bits for each.
%  
% Outputs (files written to directory "rundir") :
%  dbgHeaders_fpgaX.raw : headers, X = sourceFPGA
%  dbgData_fpgaX.raw : data, X = sourceFPGA
%
% Headers include everything from the destination MAC address to the start of the data portion.
%  This includes :
%   dest MAC : 6 bytes
%   source MAC : 6 bytes
%   ethertype : 2 bytes = 0x0800 (i.e. IPv4)
%   IPv4 Header : 20 bytes
%   UDP header : 8 bytes
%   Firmware debug mux header : 6 bytes
%     (total to this point = 6+6+2+20+8+6 = 48 bytes)
%   Firmware routing info : 8 bytes
%   Firmware packet header : 16 bytes
%     (total header length = 48+24 = 72 bytes)
% Rest of the packet is data. For LFAA ingest output, this is 8192 bytes, for a total packet length of 8192 + 72 = 8264 bytes.

modelConfig = parseModelConfig(rundir);

%% Constants
% Default header includes dest mac (d1d2d3d4d5d6), source mac (515253545556)
% ethertype (0800), IPv4 Header (), UDP header,

% destination MAC address
dbgDefaultHeader(1:6) = [hex2dec('d1'), hex2dec('d2'), hex2dec('d3'), hex2dec('d4'), hex2dec('d5'), hex2dec('d6')];  
% source MAC address
dbgDefaultHeader(7:12) = [hex2dec('51'), hex2dec('52'), hex2dec('53'), hex2dec('54'), hex2dec('55'), hex2dec('56')]; 
% ethertype
dbgDefaultHeader(13:14) = [hex2dec('08'), hex2dec('0')]; 
% IPv4 Header - first two bytes are constants
dbgDefaultHeader(15:16) = [hex2dec('45'), hex2dec('00')];
% IPv4 Header - total Length
dbgDefaultHeader(17:18) = [0,0];
% IPv4 Header - identification, flags, fragment, time to live, protocol (=0x11 = UDP)
dbgDefaultHeader(19:24) = [0, 0, 0, 0, 127, 17];
% IPv4 checksum
dbgDefaultHeader(25:26) = [0,0];
% IPv4 source IP (Note this is DHCP assigned in the hardware, so we can't know it here)
dbgDefaultHeader(27:30) = [10, 32, 0, 3];
% IPv4 Destination IP (Set by registers, may be different from this value)
dbgDefaultHeader(31:34) = [192, 168, 1, 7];
% UDP source port
dbgDefaultHeader(35:36) = [hex2dec('12'), hex2dec('34')];
% UDP destination port
dbgDefaultHeader(37:38) = [hex2dec('56'), hex2dec('79')];
% UDP length
dbgDefaultHeader(39:40) = [0, 0];
% UDP checksum
dbgDefaultHeader(41:42) = [0, 0];
% Debug mux Header
dbgDefaultHeader(43:48) = [0,0,0,0,0,0];  % Assigned depending on other parameters.
% Routing info
dbgDefaultHeader(49:56) = [0, 0, 0, 0, 0, 0, 0, 0];
% packet header (16 bytes)
dbgDefaultHeader(57:72) = 0;

%% 
sequenceNumber = 0;

if ((dbgType == 1) || (dbgType == 2) || (dbgType == 3))
    load([rundir '/stage1'],'stage1');
    
    %% Set fields in the default header for this dbgType
    % 6 byte dbg mux header : Address of this FPGA - 12 bit value, 4 bits each for X,Y,Z coordinates.
    dbgDefaultHeader(43:44) = [sourceFPGA - 1, 0];
    % interconnect source port number
    if (dbgType == 1)
        dbgDefaultHeader(45) = 18;  % Port number in the interconnect firmware module for the signal chain, 18 = even indexed packets.
    elseif (dbgType == 2)
        dbgDefaultHeader(45) = 19;  % Port number in the interconnect firmware module for the signal chain, 19 = odd indexed packets.
    end   
    
    %% First the data file, dbgData.raw
    fname = [rundir '/dbgData_fpga' num2str(sourceFPGA) '.raw'];
    fid = fopen(fname,'w');
    s1 = size(stage1(sourceFPGA).data);
    bytesPerChannel = 2*s1(1);
    thisChannel = zeros(bytesPerChannel,1);
    for b1 = 1:s1(2)
        thisChannel(1:2:end) = real(stage1(sourceFPGA).data(:,b1));  % Note stage1(XX).data is complex int8 data.
        thisChannel(2:2:end) = imag(stage1(sourceFPGA).data(:,b1));
        fwrite(fid,thisChannel,'int8');
    end
    fclose(fid);    
    
    %% Step through every packet in "stage1", and keep the ones that are going to the debug port
    % Note stage1(sourceFPGA).headers(1,p) = virtual channel for the packet
    fname = [rundir '/dbgHeaders_fpga' num2str(sourceFPGA) '.raw'];
    fid = fopen(fname,'w');
    
    packets = size(stage1(sourceFPGA).headers,2);
    hdr = zeros(104,1,'uint8');
    for p = 1:packets
        if (((dbgType == 1) && (mod(stage1(sourceFPGA).headers(1,p),2) == 0)) || ...
            ((dbgType == 2) && (mod(stage1(sourceFPGA).headers(1,p),2) == 1)) || ...
            ((dbgType == 3) && (mod(stage1(sourceFPGA).headers(1,p),modelConfig.LRUs) == (destinationFPGA-1))))
            
            % IPv4 total length = 20 (IPv4) + 8 (UDP) + 6 (dbg header) + 8 (firmware routing) + 16 (firmware header (summary of SPEAD info)) = 58 bytes of headers 
            % also 8192 bytes of data, so 8250 total = 0x203A
            dbgDefaultHeader(17) = hex2dec('20');
            dbgDefaultHeader(18) = hex2dec('3A');
            
            % UDP length = 8 + 6 (dbg header) + 8 (firmware routing) + 16 (firmware header (summary of SPEAD)) = 38 headers + 8192 data = 8230 bytes
            % 8230 = 0x2026
            dbgDefaultHeader(39) = hex2dec('20');
            dbgDefaultHeader(40) = hex2dec('26');
            
            if (dbgType == 3)
                % The port number in the interconnect firmware depends on whether the virtual channel is even or odd.
                if (mod(stage1(sourceFPGA).headers(1,p),2) == 0)
                    dbgDefaultHeader(45) = 18;
                else
                    dbgDefaultHeader(45) = 19;
                end
            end
            sequenceNumber = sequenceNumber + 1;  % Sequence starts from 1 in the firmware.
            if (sequenceNumber > 65535)
                sequenceNumber = 0;
            end
            dbgDefaultHeader(47) = floor(sequenceNumber/256);
            dbgDefaultHeader(48) = sequenceNumber - 256 * floor(sequenceNumber/256);
            
            % Firmware interconnect destination port number
            destZ = mod(stage1(sourceFPGA).headers(1,p),modelConfig.LRUs) + 1; % e.g. for PISA, destZ will be 1, 2 or 3.
            if (destZ == sourceFPGA)
                dbgDefaultHeader(46) = 18;  % 18 is the signal chain port for the corner turn module
            else
                % Must be going to somewhere on the z connect.
                % these are numbered from 0 up, with no gaps, so e.g. 
                % for PISA with 3 LRUs, the destination port will always be either 0 or 1.
                if (destZ < sourceFPGA)
                    dbgDefaultHeader(46) = destZ - 1;
                else
                    dbgDefaultHeader(46) = destZ - 2;  % -1 to convert to 0 based indexing, -1 again to account for skipping sourceFPGA in the indexing.
                end
            end
            
            % Firmware routing information
            destXYZ = destZ - 1;   % WARNING - assumes X and Y coordinates are zero.
            src1XYZ = sourceFPGA - 1;
            src2XYZ = 4095;  % 0xfff indicates unused.
            src3XYZ = 4095;  
            ptype = 1;       % Says that it comes from the LFAA signal chain.
            dbgDefaultHeader(49) = 0;
            dbgDefaultHeader(50) = ptype;
            dbgDefaultHeader(51) = mod(src3XYZ,256); % low order 8 bits
            dbgDefaultHeader(52) = floor(src3XYZ/256) * 16 + mod(src2XYZ,16);
            dbgDefaultHeader(53) = floor(src2XYZ/16);
            dbgDefaultHeader(54) = mod(src1XYZ,256);
            dbgDefaultHeader(55) = floor(src1XYZ/256) + 16 * mod(destXYZ,16);
            dbgDefaultHeader(56) = floor(destXYZ/16);
            
            % Packet Header - 32 bit packet count          (stage1(sourceFPGA).headers(4,p))
            %               - 16 bits of virtual channel    (stage1(sourceFPGA).headers(1,p))
            %               - 16 bits of channel frequency  (stage1(sourceFPGA).headers(2,p))
            %               - 16 bits station ID            (stage1(sourceFPGA).headers(3,p))
            %               - 1 bit station selection
            
            % 4 bytes of packet count
            dbgDefaultHeader(57) = mod(stage1(sourceFPGA).headers(4,p),256);   
            dbgDefaultHeader(58) = mod(floor(stage1(sourceFPGA).headers(4,p)/(256)),256);
            dbgDefaultHeader(59) = mod(floor(stage1(sourceFPGA).headers(4,p)/(256*256)),256);
            dbgDefaultHeader(60) = mod(floor(stage1(sourceFPGA).headers(4,p)/(256*256*256)),256);
            % 2 bytes virtual channel
            dbgDefaultHeader(61) = mod(stage1(sourceFPGA).headers(1,p),256);   
            dbgDefaultHeader(62) = mod(floor(stage1(sourceFPGA).headers(1,p)/(256)),256);
            % 2 byte channel frequency
            dbgDefaultHeader(63) = mod(stage1(sourceFPGA).headers(2,p),256);
            dbgDefaultHeader(64) = mod(floor(stage1(sourceFPGA).headers(2,p)/(256)),256);
            % 2 byte station ID
            dbgDefaultHeader(65) = mod(stage1(sourceFPGA).headers(3,p),256);
            dbgDefaultHeader(66) = mod(floor(stage1(sourceFPGA).headers(3,p)/(256)),256);
            % 1 byte station selection
            dbgDefaultHeader(67) = mod(stage1(sourceFPGA).headers(5,p),256);
            % Last 5 bytes are zero.
            dbgDefaultHeader(68:72) = 0;
            
            % Put this packet into the debug capture file
            % Header in the file is
            %  32 bytes of header to indicate offsets in the data file etc.
            %  72 bytes of packet data (MAC addresses, IPv4, UDP, firmware internal headers)
            % ptr(1) is the offset within the channel of the start of the data. Note this is samples (with 1 based indexing), so we need 2*(ptr(1)-1) for the offset in bytes.
            % ptr(2) is the channel. If ptr(2) = 0, the data is zeros, and we mark the byte_offset field in LFAAHeaders_fpgaX.raw as 0xffffffffffffffff
            ptr = stage1(sourceFPGA).dataPointers(p,:);
            if (ptr(2) == 0)
                hdr(1:8) = 255;
            else
                fullOffset = (ptr(2) - 1) * bytesPerChannel + (ptr(1)-1)*2;
                hdr(1:2) = 0;  % Cannot need more than 6 bytes (=262144 Gbytes)
                hdr(3) = uint8(floor(fullOffset/2^40));
                hdr(4) = uint8(mod(floor(fullOffset/2^32),256));
                hdr(5) = uint8(mod(floor(fullOffset/2^24),256));
                hdr(6) = uint8(mod(floor(fullOffset/2^16),256));
                hdr(7) = uint8(mod(floor(fullOffset/2^8),256));
                hdr(8) = uint8(mod(fullOffset,256));
            end
            % Number of bytes in the data part of the packet = 8192 = 0x2000
            hdr(9) = 0;
            hdr(10) = 0;
            hdr(11) = 32;
            hdr(12) = 0;
            % Time to send the packet in nanoseconds (Not used for debug data)
            hdr(13:20) = 0; 
            % unused
            hdr(21:32) = 0;
            % The bytes in the actual header.
            hdr(33:104) = dbgDefaultHeader;
            
            %keyboard
            fwrite(fid,uint8(hdr),'uint8');
            
        end
    end
    fclose(fid);
else
    error('Unknown debug type');
end




