{
   "global":{
      "PSSMode":[1]
   },
   
   "stations":[
      {
         "stationType":["300MHz"],
         "location":[1,2,3,4],
         "logicalIDs":[1],
         "polarisationOffsets":[2]
      }
   ],

   "subArray":[
      {
         "index":[1],
         "stationType":["300MHz"],
         "logicalIDs":[1,2,3,4]
      }
   ],

   "stationBeam":[
      {
         "index":[1],
         "subArray":[1],
         "skyIndex":[1],
         "channels":[0],
         "doppler":[0]
      }
   ],

   "correlator":[
      {
         "subArray":[1],
         "mode":["SPECTRAL_LINE"],
         "stationBeam":[1]
      }
   ],

   "PSTBeam":[
      {
         "index":[1],
         "skySubIndex":[1],
         "subArray":[1],
         "weights":[0.87, 0.67, 0.93, 0.79],
         "jonesReal":[[0.85, 0.12],[0.21, 0.92]],
         "jonesImag":[[0.45, 0.19],[0.13, 0.23]]
      },
      {
         "index":[2],
         "skySubIndex":[2],
         "subArray":[1],
         "weights":[0.68, 0.73, 0.97, 0.86],
         "jonesReal":[[0.75, 0.04],[0.11, 0.97]],
         "jonesImag":[[0.35, 0.06],[0.27, 0.20]]
      }
   ]
}
