function [doutPol0, doutPol1] = analyse_PST_HBM_data(fname,CT,Nchannels,CTLength)
% Analyse raw capture from the alveo HBM for the PST signal chain.
% PSS signal chain should be similar.
%
% Inputs :
%  fname = file name with raw capture data
%  CT = 0 for first stage corner turn, 1 for second stage corner turn
%  channels = Number of virtual channels to process from the buffer (up to 1024)
%  CTLength = length of the corner turn in LFAA blocks (standard length is 27, simulation may be shorter, e.g. 3).
%
% Output :
%  doutPol0(buffer,channel,packet,sample)
%  doutPol1(buffer,channel,packet,sample)
%    These are structures with the file content : 
%           - buffer = selects one of the 4 HBM buffers
%           - channel = virtual channel
%           - packet = select which packet (e.g. 1 to 27 for first stage corner turn, 1 to 288 for second stage corner turn)
%           - sample = time sample within an LFAA packet (for first stage corner turn), or fine frequency channel (for second stage corner turn).

% Load data
fid = fopen(fname);
rawData = fread(fid,'int8');
fclose(fid);

% Reformat into complex numbers.
rawData = rawData(1:2:end) + 1i * rawData(2:2:end);

% How many buffers are there ?
Nbuffers = ceil(length(rawData)/(2^27));  % 2^28 bytes per 256 MByte buffer = 2^27 complex numbers per buffer.

if (CT == 0)
    % First stage corner turn (raw samples from LFAA)
    doutPol0 = complex(zeros(Nbuffers,Nchannels,CTLength,2048,'int8'));
    doutPol1 = complex(zeros(Nbuffers,Nchannels,CTLength,2048,'int8'));
    for buffer = 1:Nbuffers
        for channel = 1:Nchannels
            for packet = 1:CTLength
                pstart = (buffer-1) * 2^27 + (channel-1)*2^17 + (packet-1)*4096;
                doutPol0(buffer,channel,packet,1:2048) = rawData((pstart+1):2:(pstart+4096));
                doutPol1(buffer,channel,packet,1:2048) = rawData((pstart+2):2:(pstart+4096));
            end
        end
    end
else
    % Second stage corner turn (filterbank output data)
    doutPol0 = complex(zeros(Nbuffers,Nchannels,32 * CTLength/3,216,'int8'));  % 256 point FFT with 4/3 oversampling give 32 filterbank outputs for every 3 x 2048 sample LFAA packets.
    doutPol1 = complex(zeros(Nbuffers,Nchannels,32 * CTLength/3,216,'int8'));
    for buffer = 1:Nbuffers
        for channel = 1:Nchannels
            for packet = 1:(32 * CTLength/3)
                pstart = (buffer-1) * 2^27 + (channel-1)*2^17 + (packet-1)*216 * 2;
                doutPol0(buffer,channel,packet,1:216) = rawData((pstart+1):2:(pstart+432));
                doutPol1(buffer,channel,packet,1:216) = rawData((pstart+2):2:(pstart+432));
            end
        end
    end
end








