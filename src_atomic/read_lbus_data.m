function [packets, packetsBytes, pipeline_packets, pipeline_packet_bytes] = read_lbus_data(fname, modelConfig, ipv4_dst_addr, num_pstBF_cores)
% Get data from lbus output file
%fname = 'run2\tb\lbus_out.txt';
fid = fopen(fname,'rt');

if (fid == -1)
    packets = [];
    warning('read_lbus_data : filename provided is not valid');
    return;
end

packetCount = 0;
continuingFrame = 0;

% Convert to packets with a list of bytes in each packet.
% Note : This code assumes that new packets start on a new line.
% The Lbus protocol allows packets to finish and start in the same clock cycle, but 
% the PSR packetiser doesn't do this.
while 1
    line1 = fgetl(fid);
    if ~ischar(line1)
        break
    else
        line2 = split(line1);
        % In line2 :
        %  line2{1} = repeats
        %  line2{2} = data
        %  line2{3} = valid
        %  line2{4} = eop
        %  line2{5} = errors
        %  line2{6} = empty0
        %  line2{7} = empty1
        %  line2{8} = empty2
        %  line2{9} = empty3
        %  line2{10} = sop
        if ((line2{3} ~= 'X') && (line2{3} == 'F'))
            % Check start of frame
            if (line2{10} ~= '0')
                packetCount = packetCount + 1;
                wordCount = 0;
                continuingFrame = 1;
            end
            
            %keyboard
            % Add the bytes to a byte list.
            wordCount = wordCount + 1;
            for segment = 1:4
                for byte = 1:16
                    byteData((wordCount-1)*64 + (segment-1)*16 + (byte-1) + 1) = hex2dec(line2{2}((128 - 32*(segment) + (byte-1)*2 + 1):(128 - 32*(segment) + (byte-1)*2 + 2)));
                end
            end
            
            
            % check end of frame
            if (line2{4} ~= '0') 
                continuingFrame = 0;
                packetsBytes(packetCount,1:(wordCount*64)) = byteData;
            end
        end
    end
end

fclose(fid);

nullPacket.scale = 0;
nullPacket.weights = zeros(24,1);
nullPacket.data = zeros(1536,1);
packets(1:packetCount) = nullPacket;

nullPacket_bytes.data = zeros(6336,1);

for p = 1:packetCount
    % floating point scale factor is bytes 75:78
    % relative weights is bytes 139:186
    % Packet data is bytes 187:6330
    packets(p).scale = double(typecast(uint32(sum(packetsBytes(p,75:78) .* 2.^(0:8:24))),'single'));
    for w = 1:24
        packets(p).weights(w) = packetsBytes(p,139 + 2*(w-1)) + packetsBytes(p,139 + 2*(w-1) + 1) * 256;
    end
    for sample = 1:(32*24*2*2)  % 32 times * 24 fine channels * 2 pol * 2 (complex)
        packetData(sample) = packetsBytes(p,187 + 2*(sample-1)) + packetsBytes(p,187 + 2*(sample-1) + 1) * 256;
        if (packetData(sample) > 32767)
            packetData(sample) = packetData(sample) - 65536;
        end
    end
    % rearrange into complex samples
    packets(p).data = packetData(1:2:end) + 1i * packetData(2:2:end);
    
end

num_packets_per_pipeline=zeros(1,num_pstBF_cores);

for i = 1:num_pstBF_cores
    if i~= num_pstBF_cores
       num_packets_per_pipeline(i) = floor(packetCount/length(modelConfig.channelList));
    else
       num_packets_per_pipeline(i) = packetCount - (num_pstBF_cores-1)*floor(packetCount/length(modelConfig.channelList));
    end
    pipeline_packets(i,1:num_packets_per_pipeline(i)) = nullPacket;
    pipeline_packet_bytes(i,1:num_packets_per_pipeline(i)) = nullPacket_bytes;
end

k=0;
for i = 1:num_pstBF_cores
    for p = 1:packetCount
        if packetsBytes(p,31:34) == ipv4_dst_addr(i,:)
           k=k+1;
           pipeline_packets(i,k) = packets(p);
           pipeline_packet_bytes(i,k).data = packetsBytes(p,:);
        end
    end
    k=0;
end