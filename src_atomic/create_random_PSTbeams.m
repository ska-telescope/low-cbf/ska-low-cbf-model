% write random PST beam configuration
beams = 16;
stations = 256;
fileOut = 'PSTBeamConfig.txt';
fid = fopen(fileOut,'wt');

fprintf(fid,'   "PSTBeam":[\n');

for beam = 1:beams
    fprintf(fid,'    {\n');
    fprintf(fid,['    "index":[' num2str(beam) '],\n']);
    fprintf(fid,['    "skySubIndex":[1],\n']);
    fprintf(fid,['    "subArray":[1],\n']);
    fprintf(fid,'    "weights":[');
    for station = 1:stations
        fprintf(fid,num2str(floor(rand(1)*1000)/1000));
        if station < stations
            fprintf(fid,', ');
        end
    end
    fprintf(fid,'],\n');
    
    fprintf(fid,'    "jonesReal":[[0.85, 0.12],[0.21, 0.92]],\n');
    fprintf(fid,'    "jonesImag":[[0.45, 0.19],[0.13, 0.23]]\n');
    if (beam < beams)
        fprintf(fid,'    },\n');
    else
        fprintf(fid,'    }\n');
        fprintf(fid,'    ]\n');
    end
end
