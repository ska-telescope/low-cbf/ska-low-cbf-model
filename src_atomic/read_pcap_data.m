function [packets, packetsBytes, pipeline_packets, pipeline_packet_bytes, pipeline_packets_length] = read_pcap_data(fname, ipv4_dst_addr, num_pstBF_cores)
  
  fid = fopen(fname,'rt');

  if(fid == -1)
    packets = [];
    warning('read_pcap_data : filename provided is not valid');
    return;
  end

  byte_num=0;
  packet_count=0;

  while 1
    line1 = fgetl(fid);
    if ~ischar(line1)
        break
    else
       line2 = split(line1);
       str_match = regexpi(line2{1}, '[01][0-9a-f][0-9a-f][0-9a-f]');
       if str_match == 1 
          for i=1:16
              if line2{1} == "18b0" %last line of the packet, only first 10 bytes are valid
                 if i == 11
                    packet_count = packet_count + 1;
                    packetsBytes(packet_count,1:byte_num)=bytedata;
                    byte_num = 0;
                    continue;
                 elseif i > 11
                    continue; 
                 else
                    byte_num = byte_num + 1;
                    bytedata(byte_num) = hex2dec(line2{i+1});
                 end
              else
                 byte_num = byte_num + 1;
                 bytedata(byte_num) = hex2dec(line2{i+1});
              end
          end
       end
    end
  end

  fclose(fid);

  nullPacket.scale = 0;
  nullPacket.weights = zeros(24,1);
  nullPacket.data = zeros(1536,1);
  packets(1:packet_count) = nullPacket;

  %nullPacket_bytes.data = zeros(6330,1);

  for p = 1:packet_count
      % floating point scale factor is bytes 75:78
      % relative weights is bytes 139:186
      % Packet data is bytes 187:6330
      packets(p).scale = double(typecast(uint32(sum(packetsBytes(p,75:78) .* 2.^(0:8:24))),'single'));
      for w = 1:24
          packets(p).weights(w) = packetsBytes(p,139 + 2*(w-1)) + packetsBytes(p,139 + 2*(w-1) + 1) * 256;
      end
      for sample = 1:(32*24*2*2)  % 32 times * 24 fine channels * 2 pol * 2 (complex)
          packetData(sample) = packetsBytes(p,187 + 2*(sample-1)) + packetsBytes(p,187 + 2*(sample-1) + 1) * 256;
          if (packetData(sample) > 32767)
              packetData(sample) = packetData(sample) - 65536;
          end
      end
      % rearrange into complex samples
      packets(p).data = packetData(1:2:end) + 1i * packetData(2:2:end);
  end

  num_packets_per_pipeline=zeros(1,num_pstBF_cores);

  %for i = 1:num_pstBF_cores
  %    if i~= num_pstBF_cores
  %       num_packets_per_pipeline(i) = floor(packet_count/3); %length(modelConfig.channelList));
  %    else
  %       num_packets_per_pipeline(i) = packet_count - (num_pstBF_cores-1)*floor(packet_count/3);%length(modelConfig.channelList));
  %    end
  %    disp(num_packets_per_pipeline(i));
  %    pipeline_packets(i,1:num_packets_per_pipeline(i)) = nullPacket;
  %    pipeline_packet_bytes(i,1:num_packets_per_pipeline(i)) = nullPacket_bytes;
  %end
  pipeline_packets_length=zeros(1,num_pstBF_cores);

  k=0;
  for i = 1:num_pstBF_cores
      for p = 1:packet_count
          if packetsBytes(p,31:34) == ipv4_dst_addr(i,:)
             k=k+1;
             pipeline_packets(i,k) = packets(p);
             pipeline_packet_bytes(i,k).data = packetsBytes(p,:);
          end
      end
      pipeline_packets_length(i)=k;
      k=0;
  end

end