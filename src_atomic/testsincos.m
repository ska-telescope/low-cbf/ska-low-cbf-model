lutsize = 256;
stepsize = 16384/lutsize;

for c = 1:16383
    sx1(c) = sin((c-1)/16384 * (pi/2));
    base = floor((c-1)/stepsize);  % base in the range 0 to 64
    offset = (c-1)-base*stepsize;
    sineBase = sin(base*stepsize/16384 * (pi/2));
    sineBasePlus1 = sin((base+1)*stepsize/16384 * (pi/2));
    sx2(c) = sineBase + ((sineBasePlus1 - sineBase) * offset/stepsize);    
end

figure(1);
clf;
hold on;
grid on;
plot(65536 * (sx1-sx2),'r.-');
%plot(sx2,'g.-');


