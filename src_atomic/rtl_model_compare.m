function rtl_model_compare(long_rtl_sim)
  if (long_rtl_sim == 1)	
     std_ml_fw_diff = run_PSTmodel('run14', 1, 27, '../../build/tb_result.txt', 3, 20)
  else
     std_ml_fw_diff = run_PSTmodel('run13', 1, 6, '../../build/tb_result.txt', 3, 20)	  
  end   

  for i=1:length(std_ml_fw_diff)
      if (std_ml_fw_diff(i) > 300)
         error('beam polarization combination index %d standard deviation is too high, its value is %d\n', i, std_ml_fw_diff(i));
         exit(1);
      end
  end   

  exit(0);
