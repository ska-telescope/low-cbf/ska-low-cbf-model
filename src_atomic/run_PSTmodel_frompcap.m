function run_PSTmodel_frompcap(rundir,cornerTurnFrames,cornerTurnPackets, fnameCapture, num_PSTBF_cores, RFIScale)
    % Run the low.CBF packetised model
    % 
    % ---------------------------------------------------------------------
    % Inputs
    %  
    %  * rundir : The configuration for the run is read from the run directory provided -
    %     'lmcConfig.txt' - json file with the arrayConfigFull structure, as created by create_config.m
    %     'registerSettings.txt' - json file with register settings for the run, as created by create_config.m
    %     'modelConfig.txt' - User defined json file describing the way the model is created, 
    %       e.g. which array configuration to use, amount of time to run for, which data products to keep.
    %  * cornerTurnFrames : Number of corner turns to process. While a large amount of data may be generated to 
    %      exercise the hardware, for verification purposes we usually only need to process a small portion of it.
    %  * cornerTurnPackets : Number of LFAA packets to use for the corner turn. Must be a multiple of 3.
    %     - The simulation typically uses 3 for this value, as this is the smallest value that makes sense.
    %     - The real system uses 27, which reduces the overhead for the filter preloading and gives more timing 
    %        slack for late or early arriving packets.
    %  * fnameCapture : filename for the captured data to compare with the model output.
    %  * num_PSTBF_cores : number of pipelines for multiple virutal channel parallel processing
    %  * RFIScale : Scale factor used in the firmware to prevent overflow when quantising back to 8 bit values. Nominal value is 19.
    % ----------------------------------------------------------------------
    % General structure of the code is similar to the firmware. 
    %
    %  1. Get the LFAA input data
    %  2. Filterbank + fine delay
    %  3. Beamformer
    %  4. Packetiser
    %
    % -----------------------------------------------------------------------

    %% Read the configuration files
    % Model setup
    modelConfig = parseModelConfig(rundir);

    % Firmware register settings
    if exist("OCTAVE_VERSION", "builtin") > 0
        octave_reg = loadjson([rundir '/registerSettings.txt']);
        for ii = 1:length(octave_reg.LRU)
            registers.LRU(ii) = octave_reg.LRU{ii};
        end
        registers.global = octave_reg.global;
    else
        fid = fopen([rundir '/registerSettings.txt']);
        regjson = fread(fid,inf);
        fclose(fid);
        regjson = char(regjson');
        registers = jsondecode(regjson);
    end

    % lmc Configuration
    if exist("OCTAVE_VERSION", "builtin") > 0
        arrayConfigFull = loadjson([rundir '/lmcConfig.txt']);
    else
        fid = fopen([rundir '/lmcConfig.txt']);
        lmcjson = fread(fid,inf);
        fclose(fid);
        lmcjson = char(lmcjson');
        arrayConfigFull = jsondecode(lmcjson);
    end

    if (mod(cornerTurnPackets,3) ~= 0)
        error('The number packets per corner turn must be a multiple of 3.');
    end
    
    % Other Parameters
    % Write a file to capture the output of the fine delay
    FD_CAPTURE = 1;
    FD_CAPTURE_FRAME = 1;
    FD_CAPTURE_LRU = 1;
    
    
    %% -----------------------------------------------------------------------
    % 1. LFAA ingest 
    
    % Load the data file
    fname = [rundir '/LFAA.mat'];
    if exist(fullfile(pwd,rundir,'LFAA.mat'),'file')
        load(fname);  % Should load a variable "fpga"
    else
        error(['Cannot find ' fname '. Run create_config.m to generate it']);
    end
    
    % Step through the packets, gather statistics, and form the station processing output packets.
    % The only processing is the Doppler correction.
    % The output of the station processing contains the same amount of data as the input, except that
    % the header on each packet is much smaller.

    % First, generate the output data structure
    % Output data has fields
    %  .headers : 4 x <number of input packets>. Four things in the header are as defined on the confluence page -
    %             - virtual channel
    %             - channel frequency
    %             - station ID
    %             - packet count
    %  .data    : NxM, as per the input LFAA data. N is the number of data samples, M is the number of non-zero channels.
    %  .dataPointers : <number of packets> x 2. As per fpga(lru).dataPointers. 
    %                  First column is the row index into .data,
    %                  Second column is the column index into .data. 
    %                  Note this is identical to the input structure, fpga(lru).dataPointers

    hsize = size(fpga.headers); % hsize(1) should be 114 (number of bytes in an LFAA header, hsize(2) is the number of headers.
    stage1.headers = zeros(4,hsize(2));
    usedDataSamples = cornerTurnFrames*cornerTurnPackets*2048 + 8192;  % Add 8192 samples at the front since the first stage corner turn reads from data that hasn't been generated yet.
    originalDataSamples = size(fpga.data);
    stage1.data = zeros([usedDataSamples*2,originalDataSamples(2)]);  % *2 since the polarisations are interleaved.
    stage1.data((2*8192+1):(2*8192 + 2*cornerTurnFrames*cornerTurnPackets*2048),:) = fpga.data(1:2*cornerTurnFrames*cornerTurnPackets*2048,:);  % trim the input data down to the part that we are going to process.
    stage1.dataPointers = fpga.dataPointers;
    channelDatastreamMap = zeros(1024,num_PSTBF_cores) - 1;  % map each virtual channel to a particular data stream (column) in stage1.data
    totalVirtualChannels = 0;
    totalCoarseChannels_count = 0;
    VCTABLE_regIndex = zeros(1,num_PSTBF_cores);

    for pstbf_core_idx=1:num_PSTBF_cores
        if(pstbf_core_idx==1)
          VCTABLE_regIndex(pstbf_core_idx) = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.vctable',0); 
          TOTALCHANNELS_regIndex = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.total_channels',0);
          totalVirtualChannels_perpipeline = registers.PST(TOTALCHANNELS_regIndex).value;
        else
          VCTABLE_regIndex(pstbf_core_idx) = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.vctable',pstbf_core_idx),0);
          TOTALCHANNELS_regIndex = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.total_channels',pstbf_core_idx),0);
          totalVirtualChannels_perpipeline = registers.PST(TOTALCHANNELS_regIndex).value;
        end
        totalVirtualChannels = totalVirtualChannels + totalVirtualChannels_perpipeline;
    end    
    
    packetsRequired = totalVirtualChannels * cornerTurnFrames * cornerTurnPackets;
    disp(['processing ' num2str(packetsRequired) ' LFAA packets']);
    
    for packet = 1:min(hsize(2),packetsRequired)
        pipe_idx = mod(packet,3);
        if pipe_idx == 0
           pipe_idx = 3;
        end
        fullHeader = fpga.headers(:,packet);
        % Get the relevant fields from the header and construct the header used on the internal packet.
        % bytes 1:12 - MAC addresses, bytes 13:14 - ethertype, bytes 15:34 - IPv4 Header, bytes 35:42 - UDP header
        % bytes 43:114 - SPEAD header
        SPEADHeader = fullHeader(43:114);
        % Extract the relevant fields from the SPEAD header
        frequency_id = double(SPEADHeader(55)) * 256 + double(SPEADHeader(56));
        beam_id = double(SPEADHeader(53)) * 256 + double(SPEADHeader(54));
        substation_id = double(SPEADHeader(59));
        subarray_id = double(SPEADHeader(60));
        station_id = double(SPEADHeader(61)) * 256 + double(SPEADHeader(62));
        packet_count = double(SPEADHeader(13)) * 2^24 + double(SPEADHeader(14)) * 2^16 + double(SPEADHeader(15)) * 256 + double(SPEADHeader(16));
        % Compile into a 32 bit number to compare with the entries in the virtual channel table
        virtualChannelContent = station_id * 2^21 + subarray_id * 2^16 + substation_id * 2^13 + beam_id * 2^9 + frequency_id;
        virtualChannel = find(registers.PST(VCTABLE_regIndex(pipe_idx)).value(:,1) == virtualChannelContent);

        if (length(virtualChannel) ~= 1)
           error('No Match or more than 1 match in the virtual channel table');
        end
        virtualChannel = virtualChannel - 1;

        if (channelDatastreamMap(virtualChannel+1,pipe_idx) == -1)
           channelDatastreamMap(virtualChannel+1,pipe_idx) = fpga.dataPointers(packet,2);
        elseif (channelDatastreamMap(virtualChannel+1,pipe_idx) ~= fpga.dataPointers(packet,2))
           error('data pointer is not consistent between packets for the same virtual channel.');
        end
        % Generate the internal packet header
        stage1.headers(1,packet) = virtualChannel;
        stage1.headers(2,packet) = frequency_id;
        stage1.headers(3,packet) = station_id;  % 1 based indexing of the station ID as per LFAA SPEAD, uses 10 bits for the 512 stations. (16 bits are allocated in the firmware).
        stage1.headers(4,packet) = packet_count;
    end
    
    % Save the results to stage1.mat
    %save([rundir '/stage1'], 'stage1');
    
    %% -----------------------------------------------------------------------
    % 2. Coarse corner turn, filterbanks, fine delay
    for pstbf_core_idx=1:num_PSTBF_cores
        if(pstbf_core_idx==1)
          TOTALCHANNELS_regIndex = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.total_channels',0);
          CCTDELAYTABLE_regIndex = getRegisterIndex(registers.PST,'ct_atomic_pst_in.config.table_0',0);
        else
          TOTALCHANNELS_regIndex = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.total_channels',pstbf_core_idx),0);
          CCTDELAYTABLE_regIndex = getRegisterIndex(registers.PST,sprintf('ct_atomic_pst_in_%d.config.table_0',pstbf_core_idx),0); 
        end

        totalVirtualChannels = registers.PST(TOTALCHANNELS_regIndex).value;

        % There are 32 filterbank output packets for every 3 LFAA input packets.
        fineDelay.headers = zeros(3,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);  
        % Data arranged as packets.
        fineDelay.Vdata = zeros(216,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);
        fineDelay.Hdata = zeros(216,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);
        % Fine Delay data arranged by virtual channel and fine frequency
        fineDelayVdata = zeros(totalVirtualChannels,216,cornerTurnFrames * cornerTurnPackets * 32/3); 
        fineDelayHdata = zeros(totalVirtualChannels,216,cornerTurnFrames * cornerTurnPackets * 32/3); 
    
        filterbankData.headers = zeros(3,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);  
        filterbankData.Vdata = zeros(216,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);
        filterbankData.Hdata = zeros(216,totalVirtualChannels * cornerTurnFrames * cornerTurnPackets * 32/3);
    
        %lastPtr = 1; % For storing the output of the fine delay.
        %FDCapturePtrs = zeros(128,6);
        % Filterbanks
        % Step through the channels and stations that this LRU processes
       
        CCTTable = registers.PST(CCTDELAYTABLE_regIndex).value;
        FDPacketCount = 0;
    
        for frame = 1:cornerTurnFrames   % Selects the first stage corner turn frame.
            for vc = 1:totalVirtualChannels
                %clc;
                if (mod(vc,16) == 1)
                   disp(['Filterbank processing for corner turn frame ' num2str(frame) ', virtual channel  ' num2str(vc)]);
                end
                tableSelect = 1;
                coarseDelay = mod(CCTTable((vc-1)*4 + 1,tableSelect),65536);
                hPolDeltaPCoarse = floor(CCTTable((vc-1)*4 + 1,tableSelect)/65536);
                if (hPolDeltaPCoarse > 32767)
                   hPolDeltaPCoarse = hPolDeltaPCoarse - 65536;
                end
                vPolDeltaPCoarse = mod(CCTTable((vc-1)*4 + 2,tableSelect),65536);
                if (vPolDeltaPCoarse > 32767)
                   vPolDeltaPCoarse = vPolDeltaPCoarse - 65536;
                end
                deltaDeltaPCoarse = floor(CCTTable((vc-1)*4 + 2,tableSelect)/65536);
                if (deltaDeltaPCoarse > 32767) % convert to signed.
                   deltaDeltaPCoarse = deltaDeltaPCoarse - 65536;
                end
                hPolOffset = mod(CCTTable((vc-1)*4 + 3,tableSelect),65536);
                if (hPolOffset > 32767)
                   hPolOffset = hPolOffset - 65536;
                end
                vPolOffset = floor(CCTTable((vc-1)*4 + 3,tableSelect)/65536);
                if (vPolOffset > 32767)
                   vPolOffset = vPolOffset - 65536;
                end
                OffsetStep = CCTTable((vc-1)*4 + 4,tableSelect);
                if (OffsetStep >= 2^31)  % convert to signed.
                   OffsetStep = OffsetStep - 2^32;
                end
                % First sample into the filterbank is :
                % (last sample in the previous frame) - (2048) + coarse_delay - (preload samples)
                % Preload samples = 192 * 15 = 2880, so the constant is -2048-2880 = -4928
                % There are 8192 samples of zero padding at the front of stage1.data, so that we don't get negative indices due to the preloading in the first frame.
                sampleIndex = (frame-1) * 2048 * cornerTurnPackets + coarseDelay - 4928 + 8192;   
                sampleStart = sampleIndex * 2;   % x2 because polarisations are interleaved.
            
                vPolData = double(stage1.data((sampleStart+1):2:(sampleStart + 2*(2048*cornerTurnPackets + 2880)),channelDatastreamMap(vc,pstbf_core_idx)));
                hPolData = double(stage1.data((sampleStart+2):2:(sampleStart + 2*(2048*cornerTurnPackets + 2880)),channelDatastreamMap(vc,pstbf_core_idx)));
            
                [vPolPSTFB, FFTs] = PSTfilterbank(vPolData,registers.const.PSTFilterbankTaps,1); %,256,4/3,216,512*2048);  % Last parameter is the scaling factor, which is 512 for the FIR filter and 2048 for the FFT in the firmware filterbank.
                [hPolPSTFB, FFTs] = PSTfilterbank(hPolData,registers.const.PSTFilterbankTaps,1); % ,256,4/3,216,512*2048);
            
                %keyboard
            
                for block = 1:FFTs
                
                    if (modelConfig.forceDelayZero)
                       vPolDeltaPCoarseFinal = 0;
                       hPolDeltaPCoarseFinal = 0;
                       vPolOffsetFinal = 0;
                       hPolOffsetFinal = 0;
                    else
                       % Get the number of 64 LFAA sample blocks to get to this block; 
                       % Each FFT advances by 192 samples, so 3*64 LFAA samples.
                       N_64sample_blocks = (frame-1)*cornerTurnPackets * 2048 / 64 + (block-1)*3;   
                       vPolDeltaPCoarseFinal = vPolDeltaPCoarse + N_64sample_blocks * deltaDeltaPCoarse * 2^(-15);
                       hPolDeltaPCoarseFinal = hPolDeltaPCoarse + N_64sample_blocks * deltaDeltaPCoarse * 2^(-15);
                    
                       vPolOffsetFinal = vPolOffset * 65536 + N_64sample_blocks * OffsetStep;
                       hPolOffsetFinal = hPolOffset * 65536 + N_64sample_blocks * OffsetStep;
                    end
                
                    % +/- ???  2^-31, -32
                
                    %if ((block == 24) && (frame == 1) && (vc == 1))
                    %    disp([vPolOffset hPolOffset  (vPolOffset - hPolOffset)]); 
                    %    keyboard
                    %end
                    %vPolOffsetFinal = 0;
                    %hPolOffsetFinal = 0;
                
                    vPolPhaseCorrection = exp(-1i * (2^(-32) * 2*pi * vPolOffsetFinal + 2^(-15) * 2 * pi * ((-108:107).') * vPolDeltaPCoarseFinal/128));
                    hPolPhaseCorrection = exp(-1i * (2^(-32) * 2*pi * hPolOffsetFinal + 2^(-15) * 2 * pi * ((-108:107).') * hPolDeltaPCoarseFinal/128));
                    vPolPSTFD(:,block) = vPolPSTFB(:,block) .* vPolPhaseCorrection;
                    hPolPSTFD(:,block) = hPolPSTFB(:,block) .* hPolPhaseCorrection;
                
                    %if ((vc == 4) && (block == 24))
                    %    keyboard
                    %end
                end
            
                for block = 1:FFTs
                    FDPacketCount = FDPacketCount + 1;

                    fineDelay.headers(1,FDPacketCount) = vc;      % Virtual channel (16 bits in the firmware, range 0 to 511)
                    fineDelay.headers(2,FDPacketCount) = frame;   % corner turn frame : The LFAA packet count that this came from. (Note : There are 4096 samples per filterbank output, but 2048 samples per LFAA input packet).
                    fineDelay.headers(3,FDPacketCount) = block;   % FFT block
                
                    fineDelay.Vdata(:,FDPacketCount) = vPolPSTFD(:,block);
                    fineDelay.Hdata(:,FDPacketCount) = hPolPSTFD(:,block);
                
                    % Also compile the fineDelay output into an array which is indexed as: 
                    % fineDelay.Vdata2(virtualChannel, fine frequency, time)
                    fineDelayVdata(vc,1:216,(frame-1) * FFTs + block) = vPolPSTFD(:,block);
                    fineDelayHdata(vc,1:216,(frame-1) * FFTs + block) = hPolPSTFD(:,block);
                
                    filterbankData.headers(1,FDPacketCount) = vc;      % Virtual channel (16 bits in the firmware, range 0 to 511)
                    filterbankData.headers(2,FDPacketCount) = frame;   % corner turn frame : The LFAA packet count that this came from. (Note : There are 4096 samples per filterbank output, but 2048 samples per LFAA input packet).
                    filterbankData.headers(3,FDPacketCount) = block;   % FFT block
                
                    filterbankData.Vdata(:,FDPacketCount) = vPolPSTFB(:,block);
                    filterbankData.Hdata(:,FDPacketCount) = hPolPSTFB(:,block);
                end
            end  % end of loop through channels.
        end % end of loop through frames.
    
        %% -----------------------------------------------------------------------
        % Stage3 Processing
        %  Beamformer.
    
        if(pstbf_core_idx==1)
          TOTALCOARSE_regIndex = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.total_coarse',0);
          TOTALSTATIONS_regIndex = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.total_stations',0);
          BEAMSENABLED_regIndex = getRegisterIndex(registers.PST,'ct_atomic_pst_out.statctrl.beamsenabled',0);
        else
          TOTALCOARSE_regIndex = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.total_coarse',pstbf_core_idx),0);
          TOTALSTATIONS_regIndex = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.total_stations',pstbf_core_idx),0);
          BEAMSENABLED_regIndex = getRegisterIndex(registers.PST,sprintf('ct_atomic_pst_out_%d.statctrl.beamsenabled',pstbf_core_idx),0);
        end
        totalCoarseChannels = registers.PST(TOTALCOARSE_regIndex).value;
        totalStations = registers.PST(TOTALSTATIONS_regIndex).value;
        beamsEnabled = registers.PST(BEAMSENABLED_regIndex).value;
        totalCoarseChannels_count = totalCoarseChannels_count + totalCoarseChannels;

        % Loop through groups of 32 PST time samples. There are 32 PST time samples per output packet.
        for beam = 1:beamsEnabled
        
            disp(['Calculating PST beam ' num2str(beam)]);
        
            if(pstbf_core_idx==1)
              JONESDATA_regIndex = getRegisterIndex(registers.PST,'pstbeamformer.pstbeamformer.data',(beam-1)*16384);
              PHASEDATA_regIndex = getRegisterIndex(registers.PST,'pstbeamformer.pstbeamformer.data',(beam-1)*16384 + 8192);
            else
              JONESDATA_regIndex = getRegisterIndex(registers.PST,sprintf('pstbeamformer_%d.pstbeamformer.data',pstbf_core_idx),(beam-1)*16384);
              PHASEDATA_regIndex = getRegisterIndex(registers.PST,sprintf('pstbeamformer_%d.pstbeamformer.data',pstbf_core_idx),(beam-1)*16384 + 8192);
            end
            jonesData = registers.PST(JONESDATA_regIndex).value;
            phaseData = registers.PST(PHASEDATA_regIndex).value;
        
            for frame = 1:cornerTurnFrames
                %clc;
                fprintf('in pipeline %d, calculating beam %d, frame %d\n',pstbf_core_idx, beam,frame);
                for timegroup = 1:(cornerTurnPackets/3)
                    % Every 3 LFAA packets ("cornerTurnPackets") results in 32 PST filterbank times, which are used for a single beamformed packet.
                    for coarse = 1:totalCoarseChannels
                        % Each coarse channel goes to a different output packet.
                        for fineGroup = 1:9
                            % 24 fine channels per output packet, 24*9 = 216 total fine channels per coarse channel
                            % Each output packet has (32 times) * (24 fine channels) * (2 polarisations) 
                            packetUnscaled   = zeros(32,24,2);
                            packetScaled     = zeros(32*24*2,1);
                            packetUnscaled1D = zeros(32*24*2,1);
                            for t = 1:32
                                % 32 time samples in each output packet
                                for station = 1:totalStations
                                    % Go through all the stations, weight them and add them up.
                                    virtualChannel = (coarse-1)*totalStations + (station-1) + 1;
                                    
                                    % Get the Jones Matrix
                                    jonesMatrix(1,1) = GETCMPLX(jonesData((virtualChannel-1)*4 + 1));
                                    jonesMatrix(1,2) = GETCMPLX(jonesData((virtualChannel-1)*4 + 2));
                                    jonesMatrix(2,1) = GETCMPLX(jonesData((virtualChannel-1)*4 + 3));
                                    jonesMatrix(2,2) = GETCMPLX(jonesData((virtualChannel-1)*4 + 4));

                                    % Get the phase data
                                    phaseOffset = phaseData((virtualChannel-1)*4 + 1);
                                    phaseStepFreq = phaseData((virtualChannel-1)*4 + 2);
                                    if (phaseStepFreq >= 2^31)
                                       phaseStepFreq = phaseStepFreq - 2^32;
                                    end
                                    phaseStepTime = phaseData((virtualChannel-1)*4 + 3);
                                    if (phaseStepTime >= 2^31)
                                       phaseStepTime = phaseStepTime - 2^32;
                                    end
                                    weight = phaseData((virtualChannel-1)*4 + 4);

                                    for fineChannel = 1:24
                                        dualPol(1) = fineDelayHdata(virtualChannel,(fineGroup-1)*24 + fineChannel,(frame-1)*32*(cornerTurnPackets/3) + (timegroup-1)*32 + t);
                                        dualPol(2) = fineDelayVdata(virtualChannel,(fineGroup-1)*24 + fineChannel,(frame-1)*32*(cornerTurnPackets/3) + (timegroup-1)*32 + t);
                                    
                                        jonesCorrected = jonesMatrix * round(dualPol.');
                                        % Phase correction
                                        % phaseStepFreq is the phase step per 3 fine channels.
                                        phase = phaseOffset + phaseStepTime*((frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (t-1)) + phaseStepFreq * floor(((fineGroup-1)*24 + (fineChannel-1))/3);
                                        % Firmware uses 12 bits for the phase
                                        phase12bit = floor(phase/2^20);
                                        weighted = round(jonesCorrected * exp(1i * 2*pi*phase12bit/2^12) / 64);  % firmware scale by 2^22, which includes 2^16 for the phase lookup. So we need 2^6 scaling here to be equivalent.
                                        % Accumulate
                                        packetUnscaled(t,fineChannel,:) = shiftdim(packetUnscaled(t,fineChannel,:)) + weighted;                                     
                                    end
                                end
                            end
                        
                            % Scaling in the firmware uses the abs sum of the data in the packet.
                            packetAbsSumRe = sum(sum(sum(abs(real(packetUnscaled)))));
                            packetAbsSumIm = sum(sum(sum(abs(imag(packetUnscaled)))));
                            packetAbsSum = packetAbsSumRe + packetAbsSumIm;
                        
                            % Target RMS is 360. For normally distributed data, the absolute sum is about 80% of the RMS.
                            % There are 3072 samples in a packet = (32 time) * (24 fine) * (2 pol) * (2 re+im)
                            % So the required scaling factor is
                            %  360/((packetAbsSum/3072)*5/4)
                            % The firmware uses 4 bits for the scaling factor mantissa. The mantissa is in the set (0:15)/8
                            scaleFactor = 360/((packetAbsSum/3072)*5/4);
                            scaleFactorCalculated = scaleFactor;
                       
                            % Need to round off the scale factor to make it the same as the firmware...                                              
                            % scale and repack the data in packetUnscaled into an array of 6144 16-bit numbers.
                        
                            for t = 1:32
                                for p = 1:2
                                    for fineChannel = 1:24
                                        % Packet order is time, pol, channel
                                        packetScaled((fineChannel-1)*(32*2) + (p-1)*32 + (t-1) + 1) = packetUnscaled(t,fineChannel,p) * scaleFactor;
                                        packetUnScaled1d((fineChannel-1)*(32*2) + (p-1)*32 + (t-1) + 1) = packetUnscaled(t,fineChannel,p);
                                    end
                                end
                            end

                            beamformPacket(beam,frame,timegroup,(pstbf_core_idx-1)*totalCoarseChannels+coarse,fineGroup).data = packetScaled;
                            beamformPacket(beam,frame,timegroup,(pstbf_core_idx-1)*totalCoarseChannels+coarse,fineGroup).dataUnscaled = packetUnScaled1d;
                            beamformPacket(beam,frame,timegroup,(pstbf_core_idx-1)*totalCoarseChannels+coarse,fineGroup).scale = scaleFactor;
                        end
                    end
                end
            end
        end
    end

    ipv4_dst_addr=[192 168 0 100; 192 168 0 99; 192 168 0 98];
    % Read the captured data.
    [packets,packetbytes,pipeline_packets,pipeline_packet_bytes,pipeline_packets_length] = read_pcap_data(fnameCapture, ipv4_dst_addr, num_PSTBF_cores);

    % Run through packets in the order they are produced by the firmware.
    
    % Compile data across frequency and time, into an array with dimensions (fine channels, time, polarisation, beam)
    % Note : 216 fine channels per coarse channel, 32 times per packet.
    freqTimeFW = zeros(216*totalCoarseChannels_count,cornerTurnFrames*(cornerTurnPackets/3)*32,2,beamsEnabled);
    freqTimeML = zeros(216*totalCoarseChannels_count,cornerTurnFrames*(cornerTurnPackets/3)*32,2,beamsEnabled);

    freqTimeFW_scaled = zeros(216*totalCoarseChannels_count,cornerTurnFrames*(cornerTurnPackets/3)*32,2,beamsEnabled);
    freqTimeML_scaled = zeros(216*totalCoarseChannels_count,cornerTurnFrames*(cornerTurnPackets/3)*32,2,beamsEnabled);

    beam = 1;
    
    for pipeline = 1:num_PSTBF_cores
        if pipeline==1
           TOTALCOARSE_regIndex = getRegisterIndex(registers.PST,'lfaadecode100g.statctrl.total_coarse',0);
        else
           TOTALCOARSE_regIndex = getRegisterIndex(registers.PST,sprintf('lfaadecode100g_%d.statctrl.total_coarse',pstbf_core_idx),0);
        end
        totalCoarseChannels_perpipeline = registers.PST(TOTALCOARSE_regIndex).value;
        pcount = 0;
        for frame = 1:cornerTurnFrames
            for timegroup = 1:(cornerTurnPackets/3)
                for coarse = 1:totalCoarseChannels_perpipeline
                    for fineGroup = 1:9
                        %for beam = 1:beamsEnabled, for beam 0 only
                            pcount = pcount + 1;

                            for fineChannel = 1:24
                                if ((~isempty(pipeline_packets(pipeline))) && pcount <= pipeline_packets_length(pipeline))
                                   freqTimeFW(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),1,beam) = pipeline_packets(pipeline,pcount).data(((fineChannel-1)*64 + 1):((fineChannel-1)*64 + 32)) * (512/pipeline_packets(pipeline,pcount).scale);
                                   freqTimeFW(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),2,beam) = pipeline_packets(pipeline,pcount).data(((fineChannel-1)*64 + 33):((fineChannel-1)*64 + 64)) * (512/pipeline_packets(pipeline,pcount).scale);
                                end                    
                                freqTimeML(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),1,beam) = beamformPacket(beam,frame,timegroup,(pipeline-1)*totalCoarseChannels_perpipeline+coarse,fineGroup).dataUnscaled(((fineChannel-1)*64 + 1):((fineChannel-1)*64 + 32)) * (2^(19-RFIScale));
                                freqTimeML(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),2,beam) = beamformPacket(beam,frame,timegroup,(pipeline-1)*totalCoarseChannels_perpipeline+coarse,fineGroup).dataUnscaled(((fineChannel-1)*64 + 33):((fineChannel-1)*64 + 64)) * (2^(19-RFIScale));

                                if ((~isempty(pipeline_packets(pipeline))) && pcount <= pipeline_packets_length(pipeline))
                                   freqTimeFW_scaled(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),1,beam) = pipeline_packets(pipeline,pcount).data(((fineChannel-1)*64 + 1):((fineChannel-1)*64 + 32));
                                   freqTimeFW_scaled(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),2,beam) = pipeline_packets(pipeline,pcount).data(((fineChannel-1)*64 + 33):((fineChannel-1)*64 + 64));
                                   freqTimeML_scaled(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),1,beam) = beamformPacket(beam,frame,timegroup,(pipeline-1)*totalCoarseChannels_perpipeline+coarse,fineGroup).dataUnscaled(((fineChannel-1)*64 + 1):((fineChannel-1)*64 + 32)) * (pipeline_packets(pipeline,pcount).scale/512) * (2^(19-RFIScale));
                                   freqTimeML_scaled(((pipeline-1)*totalCoarseChannels_perpipeline+(coarse-1))*216 + (fineGroup-1)*24 + fineChannel,(frame-1)*(cornerTurnPackets/3)*32 + (timegroup-1)*32 + (1:32),2,beam) = beamformPacket(beam,frame,timegroup,(pipeline-1)*totalCoarseChannels_perpipeline+coarse,fineGroup).dataUnscaled(((fineChannel-1)*64 + 33):((fineChannel-1)*64 + 64)) * (pipeline_packets(pipeline,pcount).scale/512) * (2^(19-RFIScale));
                                end
                            end
                        %end
                    end
                end
            end
        end
    end

    freqTimeMLPower = abs(freqTimeML).^2;

    if ~isempty(packets)

        disp(''); % just a new line.
        disp('Maximum and standard deviation of the difference between Matlab and firmware, for each beam and polarisation. Scaled data (i.e. as appears in the PSR packets).');
        
        display_pol = 1;
        display_beam = 1;
        %for beam = 1:beamsEnabled
            for pol = 1:2
                freqTimeDiff = abs(freqTimeML(:,:,pol,beam) - freqTimeFW(:,:,pol,beam));
                disp(['ML - FW, pol = ' num2str(pol) ', beam = ' num2str(beam) ', max diff = ' num2str(max(max(freqTimeDiff))) ', std diff = ' num2str(std(freqTimeDiff(:)))]);
                
                if ((beam == display_beam) && (pol == display_pol))
                    figure(7);
                    clf;
                    ftdiff_scaled = freqTimeDiff * 64 / max(max(freqTimeDiff));
                    image(ftdiff_scaled);
                    ylabel('frequency');
                    xlabel('time');
                    title(['ML - FW, pol = ' num2str(display_pol) ', beam = ' num2str(display_beam) ', max diff = ' num2str(max(max(freqTimeDiff))) ', std diff = ' num2str(std(freqTimeDiff(:)))]);
                    colormap jet;
                end
            end
        %end

        figure(12)
        clf;
        subplot(2,1,1)
        hold on;
        grid on;
        plot(real(freqTimeML(:,40,display_pol,display_beam)),'r.-')
        plot(real(freqTimeFW(:,40,display_pol,display_beam)),'g.-')
        xlabel('PST fine channel number, index 0-215 for coarse channel 65, index 216-431 for coarse channel 66, index 432-647 for coarse channel 67')
        ylabel('real component')
        title(['Reconstructed Beamformer output, red = matlab, green = firmware, Real part, t = 40, pol = ' num2str(display_pol) ' beam = ' num2str(display_beam)])

        subplot(2,1,2)
        hold on;
        grid on;
        plot(imag(freqTimeML(:,40,display_pol,display_beam)),'r.-')
        plot(imag(freqTimeFW(:,40,display_pol,display_beam)),'g.-')
        xlabel('PST fine channel number, index 0-215 for coarse channel 65, index 216-431 for coarse channel 66, index 432-647 for coarse channel 67')
        ylabel('imag component')
        title(['Reconstructed Beamformer output, red = matlab, green = firmware, imag part, t = 40, pol = ' num2str(display_pol) ' beam = ' num2str(display_beam)])

    end
    
    fcount = 0;
    beam = 1;
    %for beam = 1:beamsEnabled
        for pol = 1:2
            ft_scaled = freqTimeMLPower(:,:,pol,beam) * 64 / max(max(freqTimeMLPower(:,:,pol,beam)));
            fcount = fcount + 1;
            figure(7+fcount);
            clf;
            image(ft_scaled);
            ylabel('frequency');
            xlabel('time');
            title(['pol ' num2str(pol) ', beam ' num2str(beam) ', max = ' num2str(max(max(freqTimeMLPower(:,:,pol,beam))),'%10.5e')]);
            colormap jet;
        end
    %end
    
    outputfname = [rundir '\PST_results.mat'];
    
    FWpackets = packets;
    MLpackets = beamformPacket;
    save(outputfname, 'freqTimeML', 'freqTimeFW', 'FWpackets', 'MLpackets');
    
    keyboard
    
end



%% ----------------------------------------------------------------------- 
% Supporting Functions

function [dout] = correlatorFilterbank(din, FIRtaps, outputSamples)
    % Filterbank, FFT 4096 points, 12 taps.
    %FIRtaps = round(2^17 * generate_MaxFlt(4096,12));
    doRounding = 0;
    %% Pad with 11*4096 zeros to match the first output in the simulation.
    %totalSamples = length(din);
    %outputSamples = floor(totalSamples/4096);
    %dinp = zeros(totalSamples+45056,1);
    %dinp(45057:end) = din;

    %% initialise
    dout = zeros(3456,outputSamples);
    fftIn = zeros(4096,1);

    %% 
    for outputSample = 1:outputSamples
        % FIR filter, with scaling
        for n1 = 1:4096
            fftIn(n1) = sum(FIRtaps(n1:4096:end) .* din((outputSample-1)*4096 + (n1:4096:(n1+4096*11))))/2^9;
            if (doRounding)
                fftIn(n1) = round(fftIn(n1));
            end
        end

        % FFT
        dout1 = fftshift(fft(fftIn))/8192;
        dout(:,outputSample) = dout1(321:(321 + 3455));
    end
end

function [dout,outputFFTs] = PSTfilterbank(din,filterTaps,derotate)
    % Polyphase filterbank.
    % Inputs:
    %   din : input data, assumes that the leading data is initialisation data for the filterbank.
    %   filterTaps : polyphase filter taps.
    %   derotate : '1' to derotate the output.
    %
    %   fftSize : length of the FFT.
    FFTsize = 256;
    %   oversampling : oversampling ratio for the output.
    oversampling = 4/3;
    %   keep : number of frequency channels to keep (e.g. 3456 out of a 4096 point FFT).
    keep = 216;
    % scaling = output scaling factor
    scaling = 512*2048;
    % dout : output data. Length will be different to din as the outputs from the initialisation data are discarded.
    inputSamples = length(din);
    totalTaps = length(filterTaps);
    blocksPerOutput = totalTaps/FFTsize; % Number of blocks of input samples per output sample (expected to be 12)
    sampleStep = round(FFTsize / oversampling);
    outputFFTs = (inputSamples - ((blocksPerOutput) * FFTsize - sampleStep))/sampleStep; % Number of times the FFT is done
    
    dout = zeros(keep,outputFFTs);
    for f1 = 1:outputFFTs
        filtered1 = din(((f1-1)*sampleStep + 1):((f1-1)*sampleStep + blocksPerOutput*FFTsize)) .* filterTaps;
        filtered2 = reshape(filtered1,[FFTsize,blocksPerOutput]).';
        filtered3 = sum(filtered2);
        fd = fftshift(fft(filtered3));

        if (derotate == 1)
            % Derotate the output (rotation occurs due to oversampling)
            % Rotation is by pi/2, advancing with each frequency bin and time sample.
            % note : DC is at 129; no rotation; Output sample 0 has no rotation.
            % rotation defined here is in units of pi/2
            rotation = mod((f1-1) * (-128:127),4);
            fd = shiftdim(fd) .* shiftdim(exp(1i*2*pi*rotation/4));
        end
        
        % For an N point FFT, keep N/2 negative frequencies, DC, and N/2-1 positive frequencies.
        dout(:,f1) = fd((FFTsize/2 - keep/2 + 1):(FFTsize/2 + keep/2)).'; 

        %if (f1 == outputFFTs)
        %    keyboard;
        %end
    end
    dout = dout/scaling;
end




function [dout,outputFFTs] = filterbank(din,filterTaps,FFTsize,oversampling,keep,scaling)
    % Polyphase filterbank.
    % Inputs:
    %   din : input data, assumes that the leading data is initialisation data for the filterbank.
    %   filterTaps : polyphase filter taps.
    %   fftSize : length of the FFT.
    %   oversampling : oversampling ratio for the output.
    %   keep : number of frequency channels to keep (e.g. 3456 out of a 4096 point FFT).
    % dout : output data. Length will be different to din as the outputs from the initialisation data are discarded.
    inputSamples = length(din);
    totalTaps = length(filterTaps);
    blocksPerOutput = totalTaps/FFTsize; % Number of blocks of input samples per output sample (expected to be 12)
    sampleStep = round(FFTsize / oversampling);
    outputFFTs = (inputSamples - ((blocksPerOutput) * FFTsize - sampleStep))/sampleStep; % Number of times the FFT is done
    
    dout = zeros(keep,outputFFTs);
    for f1 = 1:outputFFTs
        filtered1 = din(((f1-1)*sampleStep + 1):((f1-1)*sampleStep + blocksPerOutput*FFTsize)) .* filterTaps;
        filtered2 = reshape(filtered1,[FFTsize,blocksPerOutput]).';
        filtered3 = sum(filtered2);
        fd = fftshift(fft(filtered3));
        % For an N point FFT, keep N/2 negative frequencies, DC, and N/2-1 positive frequencies.
        dout(:,f1) = fd((FFTsize/2 - keep/2 + 1):(FFTsize/2 + keep/2)).'; 
        %if (f1 == outputFFTs)
        %    keyboard;
        %end
    end
    dout = dout/scaling;
end


function [regIndex] = getRegisterIndex(namelist,name,offset)
    % Get the index of "name" in the list of strings "namelist"
    %keyboard
    found = 0;
    for i1 = 1:length(namelist)
        if ((~found) && strcmp(namelist(i1).name,name) && (namelist(i1).offset == offset))
            found = 1;
            regIndex = i1;
        end
    end
    if (~found)
        error('Register name was not found');
    end
end

function [dout] = GETCMPLX(din)
    % Convert two 16 bit values packed into 32-bit din into a complex number
    dr = mod(din,65536);
    di = floor(din/65536);
    if (dr > 32767)
        dr = dr - 65536;
    end
    if (di > 32767)
        di = di - 65536;
    end
    dout = dr + di* 1i;
end
